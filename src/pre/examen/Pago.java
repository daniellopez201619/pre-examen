package pre.examen;

public class Pago {
 //Atributos de la clase
    private String contrato;
    private String nombre;
    private String domicilio;
    private String fecha;
    private int tipo;
    private int nivel;
    private int dias;
    private float pagoBase;
    
    public Pago(){
    this.contrato="";
    this.nombre="";
    this.domicilio="";
    this.fecha="";
    this.tipo=0;
    this.nivel=0;
    this.dias=0;
    this.pagoBase=0.0f;
    }
    
    //Constructor por Argumentos
    public Pago(String contrato, String nombre, String domicilio, String fecha, int tipo, int nivel, int dias, float pagoBase) {
        this.contrato = contrato;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.fecha = fecha;
        this.tipo = tipo;
        this.nivel = nivel;
        this.dias = dias;
        this.pagoBase = pagoBase;
    }
    
    //Copia
    public Pago(Pago otro) {
        this.contrato = otro.contrato;
        this.nombre = otro.nombre;
        this.domicilio = otro.domicilio;
        this.fecha = otro.fecha;
        this.tipo = otro.tipo;
        this.nivel = otro.nivel;
        this.dias = otro.dias;
        this.pagoBase = otro.pagoBase;
    }
    
    //Metodos Set y Get
    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }
    
    //Metodos de comportamiento
    public float calcularSubTotal(){
    float subTotal=0.0f;
    
    if(nivel==1) {
    subTotal=this.dias*(this.pagoBase+this.pagoBase*0.20f);        
  }
    if(nivel==2) {
    subTotal=this.dias*(this.pagoBase+this.pagoBase*0.50f);        
  }
    if(nivel==3) {
    subTotal=this.dias*(this.pagoBase+this.pagoBase*1.0f);        
  }
    return subTotal;
    }
    
    public float calcularImpuesto(){
    float impuesto=0.0f;
    impuesto=this.calcularSubTotal()*0.16f;
    return impuesto;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularSubTotal()-this.calcularImpuesto();
    return total;
    }
}

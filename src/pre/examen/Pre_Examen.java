package pre.examen;

public class Pre_Examen {

    public static void main(String[] args) {
        //Generar objeto construido por omisicion
        
        Pago pago = new Pago();
        
        pago.setContrato("102");
        pago.setNombre("Jose Lopez");
        pago.setDomicilio("Av. del Sol 1200");
        pago.setFecha("Fecha de Pago:21 Marzo 2019");
        pago.setTipo(1);
        pago.setNivel(1);
        pago.setDias(15);
        pago.setPagoBase(700);
        
        System.out.println("Ejemplo 1:");
        System.out.println("Sub Total= "+pago.calcularSubTotal());
        System.out.println("Impuesto= "+pago.calcularImpuesto());
        System.out.println("Total= "+pago.calcularTotal());
        
        System.out.println("---------------------------------------------------------");
        
        pago.setContrato("103");
        pago.setNombre("Maria Acosta");
        pago.setDomicilio("Av. del Sol 1200");
        pago.setFecha("Fecha de Contratación:23 Marzo 2019");
        pago.setTipo(2);
        pago.setNivel(2);
        pago.setDias(15);
        pago.setPagoBase(700);
        
        System.out.println("Ejemplo 2:");
        System.out.println("Sub Total= "+pago.calcularSubTotal());
        System.out.println("Impuesto= "+pago.calcularImpuesto());
        System.out.println("Total= "+pago.calcularTotal());
    }
    
        
}
